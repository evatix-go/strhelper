package isstrs

func Empty(lines []string) bool {
	return len(lines) == 0
}
