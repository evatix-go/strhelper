package indextestwrappers

import "gitlab.com/evatix-go/core/coretests"

const (
	ofAllPtr     coretests.TestFuncName = "OfAllPtr"
	ofLastAllPtr coretests.TestFuncName = "OfLastAllPtr"
)
