package testwrappers

import "gitlab.com/evatix-go/core/coretests"

const (
	isEndsWith   coretests.TestFuncName = "IsEndsWith"
	isStartsWith coretests.TestFuncName = "IsStartsWith"
)
