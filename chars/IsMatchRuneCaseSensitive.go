package chars

func IsMatchRuneCaseSensitive(rune1 rune, rune2 rune) bool {
	return rune1 == rune2
}
