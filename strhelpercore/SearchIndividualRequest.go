package strhelpercore

type SearchIndividualRequest struct {
	Text            string
	SearchRequest   SearchRequest
	WholeTextLength int
}
