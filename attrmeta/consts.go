package attrmeta

const (
	stackTraceKey = "StackTraces"
	errorKey      = "error"
	dotError      = ".Error"
	id            = "Id"
)
