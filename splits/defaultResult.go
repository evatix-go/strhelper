package splits

import "gitlab.com/evatix-go/core/constants"

func defaultResult() []string {
	return []string{constants.EmptyString}
}
